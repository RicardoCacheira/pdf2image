const util = require('util');
const exec = require('child_process').exec;
const vm = require('vm');

class Converter {
    constructor(options = {}) {
        this.options = processOptions(options);
    }
    convertPDF(pdfFilePath) {
        return generateConvertPromise(pdfFilePath, this.options);
    }
    convertPDFList(pdfList) {
        var list = [];
        for (var pdf of pdfList) {
            list.add(generateConvertPromise(pdf, this.options));
        }
        return Promise.all(list);
    }
}

module.exports.compileConverter = function (options) {
    return new Converter(options);
}

module.exports.convertPDF = function (pdfFilePath, options = {}) {
    return generateConvertPromise(pdfFilePath, processOptions(options));
}

function generateConvertPromise(pdfFilePath = '', options) {
    pdfFilePath = pdfFilePath.toString().trim();
    if (pdfFilePath == '') {
        return new Promise(function (resolve, reject) {
            reject('Empty filePath.');
        });
    }
    return new Promise(function (resolve, reject) {
        readPDFInfo(pdfFilePath).then(function (pdfInfo) {
            var filePath = getFileDirectoryPath(pdfFilePath);
            var fileName = getFileNameFromPath(pdfFilePath);
            var pages = options.pages(pdfInfo.Pages);
            if (options.singleProcess) {
                if (pages.length > 0) {
                    processPagesSequentially(pages, pdfInfo.Pages, fileName, filePath, options, pdfFilePath, resolve, reject, 0);
                } else {
                    resolve([]);
                }
            } else {
                var promises = [];
                for (var i = 0; i < pages.length; i++) {
                    promises.push(createPagePromise(pages[i], i + 1, pages.length, pdfInfo.Pages, fileName, filePath, options, pdfFilePath));
                }
                Promise.all(promises).then(function (list) {
                    resolve(list);
                }, function (errorList) {
                    reject(errorList);
                });
            }

        }, function (error) {
            reject(error);
        })
    });
}

function processPagesSequentially(pageList, totalPDFPages, fileName, filePath, options, pdfFilePath, resolve, reject, i, resultList = []) {
    createPagePromise(pageList[i], i + 1, pageList.length, totalPDFPages, fileName, filePath, options, pdfFilePath).then(function (result) {
        resultList.push(result);
        if (pageList.length > (i + 1)) {
            processPagesSequentially(pageList, totalPDFPages, fileName, filePath, options, pdfFilePath, resolve, reject, i + 1, resultList);
        } else {
            resolve(resultList);
        }
    }, function (error) {
        reject(error);
    });
}

function getFileDirectoryPath(filePath) {
    return filePath.substring(0, filePath.lastIndexOf('/'));
}

function getFileNameFromPath(filePath) {
    var aux = filePath.split('/').pop().split('.')
    return aux[0] != '' ? aux[0] : aux[1];
}

/**

A token starts with the character '%'

d - the page number, if the first page of the PDF is page 1
D - the page number, if the first page of the PDF is page 0
i - the processed page number, if the first processed page is page 1
I - the processed page number, if the first processed page is page 0
t - the total number of pages in the pdf
T - the total of processed pages
s - the name of the pdf file
p - the path of the pdf file
% - the character '%'
{...} - a custom piece of code where all of the above values can be used
 */
function generateOutputFormatFunction(outputFormatString) {
    var tokenList = [];
    var unreadPos = 0;
    var context = '';
    for (var i = 0; i < outputFormatString.length; i++) {
        if (outputFormatString.charAt(i) == '%') {
            tokenList.push("'" + outputFormatString.substring(unreadPos, i) + "'");
            i += 1 //bypasses the identifier

            switch (outputFormatString.charAt(i)) {
                case 'd':
                    tokenList.push("pageNum");
                    break;
                case 'D':
                    tokenList.push("(pageNum-1)");
                    break;
                case 'i':
                    tokenList.push("pageIndex");
                    break;
                case 'I':
                    tokenList.push("(pageIndex-1)");
                    break;
                case 's':
                    tokenList.push("name");
                    break;
                case 'p':
                    tokenList.push("path");
                    break;
                case '%':
                    tokenList.push("'%'");
                    break;
                case 't':
                    tokenList.push("totalPDFPages");
                    break;
                case 'T':
                    tokenList.push("totalPagesProcessed");
                    break;
                case '{':
                    var start = i + 1;
                    var remainingBrackets = 0;
                    while (outputFormatString.charAt(i)) {
                        if (outputFormatString.charAt(i) != '}') {
                            i++;
                        } else {
                            if (remainingBrackets) {
                                remainingBrackets--;
                                i++;
                            } else {
                                break;
                            }
                        }
                        if (outputFormatString.charAt(i) == '{') {
                            remainingBrackets++;
                        }
                    }
                    if (outputFormatString.charAt(i)) {
                        tokenList.push("vm.runInContext('" + outputFormatString.substring(start, i) + "',context)");
                        if (!context) {
                            context = 'var context=vm.createContext({d:pageNum,' +
                                'D:pageNum-1,' +
                                'i:pageIndex,' +
                                'I:pageIndex-1,' +
                                's:name,' +
                                'p:path,' +
                                't:totalPDFPages,' +
                                'T:totalPagesProcessed});';
                        }
                    }
                    break;
                default:
                    break;
            }
            unreadPos = i + 1;
        }
    }
    tokenList.push("'" + outputFormatString.substring(unreadPos, outputFormatString.length) + "'");
    return new Function('pageNum', 'pageIndex', 'totalPagesProcessed', 'totalPDFPages', 'name', 'path', 'vm', context + 'return ""+' + tokenList.join('+'));
}

function processOptions(receivedOptions) {
    var options = {};
    options.outputType = processOutputType(receivedOptions.outputType);

    if (receivedOptions.density) {
        options.density = positiveIntOrDefault(receivedOptions.density, 96);
    } else if (receivedOptions.width || receivedOptions.height) {
        options.width = positiveIntOrDefault(receivedOptions.width, '');
        options.height = positiveIntOrDefault(receivedOptions.height, '');
    } else {
        options.density = 96;
    }

    //Its a jpg file
    if (options.outputType == '.jpg') {
        //quality is only relevant to jpg files
        options.quality = positiveIntOrDefault(receivedOptions.quality, 100);
        if (options.quality > 100) {
            options.quality = 100;
        }
    }
    switch (typeof receivedOptions.outputFormat) {
        case 'function':
            options.outputFormat = receivedOptions.outputFormat;
        case 'string':
            options.outputFormat = generateOutputFormatFunction(receivedOptions.outputFormat);
            break;
        default: //is the same as '%d'
            options.outputFormat = function (pageNum) {
                return pageNum.toString();
            };
    }
    options.pages = receivedOptions.pages ? processPages(receivedOptions.pages.toString()) : processPages('*');
    options.singleProcess = !!receivedOptions.singleProcess;
    options.backgroundColor = processBackgroundColor(receivedOptions.backgroundColor);
    if (options.backgroundColor == '' && options.outputType == '.jpg') {
        options.backgroundColor = '"#FFFFFF"';
    }
    return options;
}

function processOutputType(type = '') {
    type = type.toString();
    if (type.charAt(0) == '.') {
        type = type.substring(1)
    }
    type = type.toLowerCase();
    if (type == 'png') {
        return '.png';
    } else {
        return '.jpg';
    }
}

function isHex(char) {
    return char == '0' ||
        char == '1' ||
        char == '2' ||
        char == '3' ||
        char == '4' ||
        char == '5' ||
        char == '6' ||
        char == '7' ||
        char == '8' ||
        char == '9' ||
        char == 'a' ||
        char == 'b' ||
        char == 'c' ||
        char == 'd' ||
        char == 'e' ||
        char == 'f' ||
        char == 'A' ||
        char == 'B' ||
        char == 'C' ||
        char == 'D' ||
        char == 'E' ||
        char == 'F';
}

function processBackgroundColor(color) {
    if (typeof color == 'string') {
        if (color.charAt(0) != '#') {
            color += '#';
        }
        if (color.length == 7) {
            var validHex = true;
            for (var i = 1; i < 7; i++) {
                if (!isHex(color.charAt(i))) {
                    validHex = false;
                    break;
                }
            }
            if (validHex) {
                return '"' + color + '"';
            }
        }
    }
    return ''
}

function createPagePromise(pageNum, pageIndex, totalPagesProcessed, totalPDFPages, name, path, options, pdfFilePath) {
    return new Promise(function (resolve, reject) {
        var outputString = options.outputFormat(pageNum, pageIndex, totalPagesProcessed, totalPDFPages, name, path + (path ? '/' : ''), vm) + options.outputType;
        var convertOptions = [];
        if (options.density) {
            convertOptions.push('-density ' + options.density)
        } else {
            convertOptions.push(imgSize = '-resize ' + options.width + (options.height ? 'X' + options.height : ""));
        }
        if (options.quality) {
            convertOptions.push('-quality ' + options.quality);
        }
        if (options.backgroundColor != '') {
            convertOptions.push('-background ' + options.backgroundColor + ' -flatten');
        }

        aux = util.format('convert %s %s[%d] %s', convertOptions.join(' '), pdfFilePath, pageNum - 1, outputString);
        exec(aux, function (err, stdout, stderr) {
            if (err) {
                reject(Error(err));
            } else {
                resolve({
                    page: pageNum,
                    index: pageIndex,
                    name: outputString.split('/').pop(),
                    path: outputString
                });
            }
        });
    });
}

/*

* -> everything

1 -> page 1

3-5 -> pages 3 to 5

-7 -> pages 1 to 7

6- -> pages 6 to the last

/5 -> all values divisable by 5

even -> all even numbers

odd -> all odd numbers

*/
function processPages(pagesStr) {
    if (pagesStr === '*') {
        return function (length) {
            var pages = [];
            for (var i = 1; i <= length; i++) {
                pages.push(i);
            }
            return pages;
        };
    } else {
        var aux = pagesStr.toString().split(',');
        var ruleList = [];
        for (var option of aux) {
            if (option != '') {
                //TODO improve detection of options 
                //fazer que opcoes repetidas nao sejam inseridas
                if (option === 'even') {
                    ruleList.push({
                        type: 'even'
                    });
                } else if (option === 'odd') {
                    ruleList.push({
                        type: 'odd'
                    });
                } else if (option.charAt(0) == '/') {
                    var x = positiveIntOrDefault(option.substring(1), undefined);
                    if (x) {
                        ruleList.push({
                            type: 'multiple',
                            value: x
                        });
                    }
                } else {
                    var x = option.split('-');
                    if (x.length == 1) {
                        x = positiveIntOrDefault(x[0], undefined);
                        if (x) {
                            ruleList.push({
                                type: 'page',
                                value: x
                            });
                        }
                    } else if (x.length == 2) {
                        if (x[0] == '') {
                            x[0] = '1';
                        }
                        x[0] = positiveIntOrDefault(x[0], undefined);
                        if (x[0]) {
                            if (x[1] == '') {
                                ruleList.push({
                                    type: 'range-max',
                                    start: x[0]
                                });
                            } else {
                                x[1] = positiveIntOrDefault(x[1], undefined);
                                if (x[1]) {
                                    //fazer que se forem trocados ficarem direitos
                                    if (x[0] < x[1]) {
                                        ruleList.push({
                                            type: 'range',
                                            start: x[0],
                                            end: x[1]
                                        });
                                    } else if (x[0] > x[1]) {
                                        ruleList.push({
                                            type: 'range',
                                            start: x[1],
                                            end: x[0]
                                        });
                                    } else {
                                        ruleList.push({
                                            type: 'page',
                                            value: x[0]
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return function (length) {
            return calculatePages(ruleList, length).sort(function (a, b) {
                return a - b
            });
        };
    }
}

function calculatePages(ruleList, max) {
    var pages = [];
    var pushVal = function (x) {
        if (pages.indexOf(x) == -1) {
            pages.push(x)
        }
    };
    for (var opc of ruleList) {
        switch (opc.type) {
            case 'page':
                if (opc.value <= max) {
                    pushVal(opc.value);
                }
                break;
            case 'even':
                for (var i = 2; i <= max; i++) {
                    if (i % 2 == 0) {
                        pushVal(i);
                    }
                }
                break;
            case 'odd':
                for (var i = 1; i <= max; i++) {
                    if ((i + 1) % 2 == 0) {
                        pushVal(i);
                    }
                }
                break;
            case 'multiple':
                for (var i = 1; i <= max; i++) {
                    if (i % opc.value == 0) {
                        pushVal(i);
                    }
                }
                break;
            case 'range':
                var x = opc.end < max ? opc.end : max;
                for (var i = opc.start; i <= x; i++) {
                    pushVal(i);
                }
                break;
            case 'range-max':
                for (var i = opc.start; i <= max; i++) {
                    pushVal(i);
                }
                break;
        }
    }
    return pages;
}

function positiveIntOrDefault(value, defaultValue) {
    if (value) {
        value = Number.parseInt(value);
        if (Number.isSafeInteger(value) && value > 0) {
            return value;
        }
    }
    return defaultValue;
}

function readPDFInfo(pdfFilePath) {
    return new Promise(function (resolve, reject) {
        exec('pdfinfo ' + pdfFilePath, function (error, stdout, stderr) {
            if (error !== null) {
                reject(Error('Error reading pdf file.'));
            } else {
                var pdfInfo = {};
                var infoSplit = stdout.split('\n');
                for (var line of infoSplit) {
                    var aux = line.split(':');
                    pdfInfo[aux[0].trim()] = aux[1] ? aux[1].trim() : '';
                }
                resolve(pdfInfo)
            }
        });
    });
}