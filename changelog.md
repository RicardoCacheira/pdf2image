1.2.3 - 2017/07/28
=====

* Added the ability to define the background color of the processed pages.


1.2.2 - 2017/07/03
=====

* Added missing '}'.

1.2.1 - 2017/07/03
=====

* Fixed a bug where the code token failed if used.
* Made the range in the pages options work even if the last value was less than the first (e.g. 7-2 will work the same as 2-7 instead of being ignored).

1.2.0 - 2017/07/02
=====

* Made the return list of the promise be ordered by page number.
* Added the ability to define a function to generate the output path instead of a string.
* Added new possible tokens to the outputFormat string.
* Removed the '%/' token from the outputFormat.
* Changed the path token (%p) to also contain the final '/' if it exists instead of being a seperate token(a.k.a. the '%/' token).
* Removed the restriction of a token only be able to be used one time on the outputFormat string.
* Fixed a bug where the last processed page in the sequencial mode would not be added the the result list.

1.1.0 - 2017/04/07
=====

* Added the ability to force only a page to be processed at a time.
* Added the ability to define the width and the height of the generated pages.